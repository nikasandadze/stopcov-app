package com.example.finalexam.adapters

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.finalexam.fragments.fragment1
import com.example.finalexam.fragments.fragment2
import com.example.finalexam.fragments.fragment3
import com.example.finalexam.fragments.fragment4

class ViewPagerFragmentAdapter(activity: AppCompatActivity) : FragmentStateAdapter(activity) {
    override fun getItemCount() = 4

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0->{
                fragment1()
            }
            1->{
                fragment2()
            }
            2->{
                fragment3()
            }
            3->{
                fragment4()
            }
            else->{
                Fragment()
            }
        }
    }
}